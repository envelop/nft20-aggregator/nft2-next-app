"use client";

import { useState, useEffect } from 'react';

const CardList = ({ data }) => {
    return (
        <>
        { data.map((item) => (
            item.title
        ))}
        </>
    )
}

const Cards = () => {

    const [items, setItems] = useState([]);

    useEffect(() => {
        const fetchCards = async () => {
            const response = await fetch ('/api/cards');
            const data = await response.json();
            setItems(data);
        }
        fetchCards();
    }, []);

    return (
        <CardList
            data={items}
        />
    )
}

export default Cards;