import Header from '../../components/header.component';
import Footer from '../../components/footer.component';
import Projects from '../../components/projects.component';

export default function Home() {
  return (
    <div className="wrapper">
      <header className="s-header">
        <Header />
      </header>
      <main className="s-main">
        <Projects />
      </main>
      <footer className="s-footer">
        <Footer />
      </footer>
    </div>
  );
}
