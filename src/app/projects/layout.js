
export const metadata = {
  title: "Projects on NFT 2.0 Aggregator by Envelop",
  description: "NFT projects",
};

export default function RootLayout({
  children,
}) {
  return (
    <>{children}</>
  );
}
