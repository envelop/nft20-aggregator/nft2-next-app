import Header from '../components/header.component';
import Footer from '../components/footer.component';
import Intro from '../components/intro.component';
import Cards from '../components/cards.component';
import Video from '../components/video.component';
import Form from '../components/form.component';

export default function Home() {

  return (
    <div className="wrapper">
      <header className="s-header">
        <Header />
      </header>
      <main className="s-main">
        <Intro />
        <Cards />
        <div className="divider right"></div>
        <Video />
        <div className="divider left"></div>
        <Form />
      </main>
      <footer className="s-footer">
        <Footer />
      </footer>
    </div>
  );
}
