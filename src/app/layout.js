import '../scss/styles.scss';

export const metadata = {
  title: "NFT 2.0 Aggregator by Envelop",
  description: "The service collects data on all projects in the NFT 2.0 sphere, also known as Utility NFTs, Smart NFTs, and programmable NFTs.",
};

export default function RootLayout({
  children,
}) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  );
}
