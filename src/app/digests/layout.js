
export const metadata = {
  title: "Digests on NFT 2.0 by Envelop",
  description: "The most complete NFT market insights for fans, researchers, writers and degens in the monthly NFT Digest.",
};

export default function RootLayout({
  children,
}) {
  // if (typeof window === 'undefined') return null;
  return (
    <>{children}</>
  );
}
