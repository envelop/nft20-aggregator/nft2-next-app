
export const metadata = {
  title: "Books on NFT 2.0 by Envelop",
  description: "Check out our books with NFT market insights.",
};

export default function RootLayout({
  children,
}) {
  return (
    <>{children}</>
  );
}
