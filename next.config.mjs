/** @type {import('next').NextConfig} */
const nextConfig = {
    output: 'export',
    images: {
        unoptimized: true,
        remotePatterns: [{
        protocol: 'https',
        hostname: 'stage.nft2.envelop.is',
        port: '',
        pathname: '/sites/default/files/styles/**'
        }]
    }
};
export default nextConfig;
